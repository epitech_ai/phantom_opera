def accuracy(file, type):
    file = open(file, 'r')#'win_records_phantom_random_inspector_ai', 'r')
    lines = file.readlines()
    total = 0
    nb_0 = 0
    nb_1 = 0
    for line in lines:
        nb = int(line)
        if nb == 0:
            nb_0 += 1
        if nb == 1:
            nb_1 += 1
        total += 1
    print(str(type) + "Loose Accuracy", nb_0 / total * 100, "%")
    print(str(type) + "Win Accuracy", nb_1 / total * 100, "%")


print("\n")
accuracy("win_records_phantom_random_inspector_ai", "Inspector ")
print("\n")
accuracy('win_records_inspector_random_phantom_ai', "Phantom ")
print("\n")
accuracy('win_records_inspector_random_phantom_ai_heurisitc2', "Phantom heuristic update v2 ")
print("\n")
accuracy('win_recors_ai_vs_ai_fantom_1', "Phantom vs Inspector ")
