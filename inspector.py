import socket
import os
import logging
from logging.handlers import RotatingFileHandler
import json
import protocol
from random import randrange
import random
import ai_engine

host = "localhost"
port = 12000
# HEADERSIZE = 10

"""
set up inspector logging
"""
inspector_logger = logging.getLogger()
inspector_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s :: %(levelname)s :: %(message)s", "%H:%M:%S")
# file
if os.path.exists("./logs/inspector.log"):
    os.remove("./logs/inspector.log")
file_handler = RotatingFileHandler('./logs/inspector.log', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
inspector_logger.addHandler(file_handler)
# stream
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.WARNING)
inspector_logger.addHandler(stream_handler)

import copy

class Player():

    def __init__(self):

        self.end = False
        # self.old_question = ""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.currentListOfActions = []
        self.currentActionId = -1

    def connect(self):
        self.socket.connect((host, port))

    def reset(self):
        self.socket.close()

    def action_choose(self, actionId, data):
        action = self.currentListOfActions[actionId]
        print(self.currentListOfActions[actionId])
        print(data)
        if action['type'] == 'cardId':
            return action['value']

        if action['type'] == 'caseId':
            try:
                return data.index(action['value'])
            except Exception as e:
                pass
        if action['value'] == -1 or action['type'] == 'undefined':
            return random.randint(0, len(data)-1)

        if action['type'] == 'choice':
            print("choice")
            return action['value']

        if action['type'] == 'playerSelection':
            try:
                return data.index(action['value'])
            except Exception as e:
                pass

        return random.randint(0, len(data) - 1)



    def answer(self, question):
        # work
        data = question["data"]
        game_state = question["game state"]
        response_index = random.randint(0, len(data)-1)
        print(json.dumps(question, indent=4, sort_keys=True))

        self.currentActionId += 1
        if question['question type'] == "select character":
            self.currentListOfActions = None
            self.currentActionId = 0
        if not self.currentListOfActions:
            ai_result = ai_engine.generate_leaf(copy.deepcopy(question))
            self.currentListOfActions = ai_result[0]['actions']
        try:
            response_index = self.action_choose(self.currentActionId, question["data"])
        except:
            response_index = random.randint(0, len(data) - 1)
        # pass the inspector in random mod
        #response_index = random.randint(0, len(data) - 1)
        print("Response index", response_index)
        #response_index = int(input("Enter value "))

        # log
        inspector_logger.debug("|\n|")
        inspector_logger.debug("inspector answers")
        inspector_logger.debug(f"question type ----- {question['question type']}")
        inspector_logger.debug(f"data -------------- {data}")
        inspector_logger.debug(f"response index ---- {response_index}")
        inspector_logger.debug(f"response ---------- {data[response_index]}")
        return response_index

    def handle_json(self, data):
        data = json.loads(data)
        response = self.answer(data)
        # send back to server
        bytes_data = json.dumps(response).encode("utf-8")
        protocol.send_json(self.socket, bytes_data)

    def run(self):

        self.connect()

        while self.end is not True:
            received_message = protocol.receive_json(self.socket)
            if received_message:
                self.handle_json(received_message)
            else:
                print("no message, finished learning")
                self.end = True


p = Player()

p.run()
