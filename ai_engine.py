import json
import tkinter
from itertools import product

input = {
    "data": [
        {
            "color": "blue",
            "position": 2,
            "power": True,
            "suspect": True
        },
        {
            "color": "grey",
            "position": 2,
            "power": True,
            "suspect": True
        },
        {
            "color": "black",
            "position": 3,
            "power": True,
            "suspect": True
        },
        {
            "color": "white",
            "position": 2,
            "power": True,
            "suspect": True
        }
    ],
    "game state": {
        "active tiles": [
            {
                "color": "blue",
                "position": 2,
                "power": True,
                "suspect": True
            },
            {
                "color": "grey",
                "position": 2,
                "power": True,
                "suspect": True
            },
            {
                "color": "black",
                "position": 3,
                "power": True,
                "suspect": True
            },
            {
                "color": "white",
                "position": 2,
                "power": True,
                "suspect": True
            }
        ],
        "blocked": [
            0,
            1
        ],
        "characters": [
            {
                "color": "white",
                "position": 2,
                "power": True,
                "suspect": True
            },
            {
                "color": "blue",
                "position": 2,
                "power": True,
                "suspect": True
            },
            {
                "color": "red",
                "position": 5,
                "power": True,
                "suspect": True
            },
            {
                "color": "brown",
                "position": 0,
                "power": True,
                "suspect": True
            },
            {
                "color": "pink",
                "position": 6,
                "power": True,
                "suspect": True
            },
            {
                "color": "grey",
                "position": 2,
                "power": True,
                "suspect": True
            },
            {
                "color": "purple",
                "position": 4,
                "power": True,
                "suspect": True
            },
            {
                "color": "black",
                "position": 3,
                "power": True,
                "suspect": True
            }
        ],
        "exit": 22,
        "fantom": "brown",
        "num_tour": 1,
        "position_carlotta": 4,
        "shadow": 9
    },
    "question type": "select character"
}

permanents, two, before, after = ['pink'], \
                                 ['red', 'grey', 'blue'], \
                                 ['purple', 'brown'], \
                                 ['black', 'white']
# reunion of sets
colors = permanents + two + before + after
passages = [[1, 4],
            [0, 2],
            [1, 3],
            [2, 7],
            [0, 5, 8],
            [4, 6],
            [5, 7],
            [3, 6, 9],
            [4, 9],
            [7, 8]]
# ways for the pink character
pink_passages = [[1, 4],
                 [0, 2, 5, 7],
                 [1, 3, 6],
                 [2, 7],
                 [0, 5, 8, 9],
                 [4, 6, 1, 8],
                 [5, 7, 2, 9],
                 [3, 6, 9, 1],
                 [4, 9, 5],
                 [7, 8, 4, 6]]


def get_destination_cases(player_position, cardColor=None):
    if cardColor and cardColor == 'pink':
        return pink_passages[player_position]
    return passages[player_position]


# noir
# selection position
# activate black power
# if yes


# bleu
# activate blue power ?
# if yes
## blu character power room choose between 0 and 9

# brown
# activate brown power ?
# select position ?
#

# red
# activate red power ?
# move ?
# activate red power ?

# pink
# position

# asked player id
# asked activate power

def copy_except(cards, except_card):
    res = []
    for x in cards:
        if x['color'] != except_card['color']:
            res.append(x)
    return res


def update_position(players, player_color, player_new_position):
    res = []
    for player in players:
        if player['color'] == player_color:
            player['position'] = player_new_position
        res.append(player)
    return res


import copy


def switch_player_position(player_origin_color, player_dest_color, players):
    v_player_origin = -1
    v_player_dest = -1
    for player in players:
        if player['color'] == player_origin_color:
            v_player_origin = player['position']
        if player['color'] == player_dest_color:
            v_player_dest = player['position']

    players_tmp = copy.deepcopy(players)
    for player in players_tmp:
        if player['color'] == player_origin_color:
            player['position'] = v_player_dest
        if player['color'] == player_dest_color:
            player['position'] = v_player_origin
    return players_tmp

def find_player_position(players, player_color):
    for player in players:
        if player['color'] == player_color:
            return player['position']

def power_actions(player, gameState):
    # ret values
    actions = []
    newGameStates = []

    newGameState = copy.deepcopy(gameState)

    if player['color'] == "blue":  # Giry: peut verrouiller les portes
        for x in range(9):
            if x not in newGameState["blocked"]:
                actionsTmp = []
                actionsTmp.append({
                    'value': x,
                    'type': 'caseId',
                    'msg': 'choose the case input block'
                })

                for x_dest in passages[x]:
                    if x in newGameState["blocked"] and x_dest in newGameState["blocked"]:
                        continue
                    actionsTmp2 = copy.deepcopy(actionsTmp)
                    actionsTmp2.append({
                        'value': x_dest,
                        'type': 'caseId',
                        'msg': 'choose the case output block'
                    })
                    actions.append(actionsTmp2)
                    tmpGameState = copy.deepcopy(newGameState)
                    tmpGameState['blocked'] = [x, x_dest]
                    newGameStates.append(tmpGameState)

    if player['color'] == 'purple':  # (PURPLE) M. Richard: peut echanger sa position avec un autre personnage
        for otherPlayer in gameState['characters']:
            if player['color'] != otherPlayer['color']:
                actionsTmp = copy.deepcopy(actions)
                actionsTmp.append({
                    'value': otherPlayer['color'],
                    'type': 'playerSelection',
                    'msg': 'choose the player you want to swap the position'
                })
                tmpGameState = copy.deepcopy(newGameState)
                tmpGameState['characters'] = switch_player_position(player['color'], otherPlayer['color'],
                                                                    tmpGameState['characters'])
                # add to the final result
                actions.append(actionsTmp)
                newGameStates.append(tmpGameState)

    if player['color'] == 'black':  # (BLACK) Christine Daae: peut attirer d'autres personnages dans la meme piece que la sienne
        tmpGameState = copy.deepcopy(gameState)
        for x in passages[player['position']]:
            for otherPlayer in tmpGameState['characters']:
                if otherPlayer['color'] != player['color'] and \
                        otherPlayer['position'] != player['position'] and \
                        otherPlayer['position'] == x:
                    otherPlayer['position'] = player['position']

        actions.append({'value': 1, 'type': 'choice', 'msg': 'no actions black power'})
        newGameStates.append(tmpGameState)

    if player['color'] == 'pink':  # no power
        pass

    if player['color'] == 'brown':  # (BROWN) Le Persan: peut prendre un autre personnage avec lui
        action = []
        action.append({
            'value': -1,
            'msg': 'random',
            'type': 'undefined'
        })
        actions.append(action)
        nGS = copy.deepcopy(gameState)
        newGameStates.append(nGS)

    if player['color'] == 'white':  # (WHITE) M. Moncharmin: peut faire fuir un mec de sa chambre par d'autres personnages
        tmpGameState = copy.deepcopy(gameState)
        valid_x = []
        for x in passages[player['position']]:
            if x in tmpGameState['blocked'] and player['position'] in tmpGameState['blocked']:
                continue
            else:
                valid_x.append(x)
        players_in = []
        # get player in the case
        for pp in tmpGameState['characters']:
            if pp['position'] == player['position'] and not pp['color'] == 'white':
                players_in.append(pp)

        # initialize N
        N = len(players_in)
        # All possible N combination tuples
        possibilities = [ele for ele in product(valid_x, repeat=N)]

        for possibility in possibilities:
            action = []
            nGS = copy.deepcopy(gameState)
            for destCaseId, destCase in enumerate(possibility):
                # create the action
                action.append({
                    'value': destCase,
                    'type': 'caseId',
                    'msg': 'choose the case where move the player'
                })
                # udpate player position
                update_position(nGS['characters'], players_in[destCaseId]['color'], destCase)

            actions.append(action)
            newGameStates.append(nGS)

    if player['color'] == 'red':  # (RED) Raoul de Chagny: peut piocher une carte alibi
        pass

    if player['color'] == 'grey':  # (GRIS) Joseph Buquet: peut eteindre la lumiere
        for x in range(9):
            tmpGameState = copy.deepcopy(gameState)
            tmpGameState['shadow'] = x
            actions.append({'value': x, 'type': 'caseId', 'msg': 'action choose a black room'})
            newGameStates.append(tmpGameState)

    return actions, newGameStates

def generate_leaf(input, phantom=False):
    leafs = []
    item = {
        'actions': [],
        'gamestate': {}
    }
    # for each player
    for cardId, card in enumerate(input["game state"]['active tiles']):
        print(card)
        cardActions = [{"value": cardId, "type": 'cardId', 'msg': 'choose card'}]

        def gen_move(previousActions, gameStateIn):
            for moveId, moveCaseId in enumerate(get_destination_cases(find_player_position(gameStateIn["characters"], card['color']), card['color'])):
                if moveCaseId in gameStateIn['blocked'] and\
                        find_player_position(gameStateIn["characters"], card['color']) in gameStateIn['blocked']:
                    continue
                # no power
                valuetmp = copy.deepcopy(previousActions)
                valuetmp.append({'value': moveCaseId, 'type': 'caseId', 'msg': 'move to the case'})

                gameStatett = copy.deepcopy(input)
                gameStatett["game state"]["characters"] = gameStateIn["characters"]
                gameStatett["game state"]["characters"] = update_position(
                    copy.deepcopy(gameStatett["game state"]["characters"]),
                    card['color'],
                    moveCaseId)
                gameStatett['game state']['active tiles'] = copy_except(copy.deepcopy(input['game state']['active tiles']), card)
                leafs.append({
                    'actions': valuetmp,
                    'gamestate': gameStatett
                })

        if card['color'] == 'pink':
           gen_move(copy.deepcopy(cardActions), copy.deepcopy(input['game state']))
           continue

        if card["color"] in two:
            """
            POWER BEFORE
            """
            # root with the choosen card
            newActions = copy.deepcopy(cardActions)

            # generate activation first
            newActions.append({"value": 1, "type": "choice", "msg": 'before YES power activation'})

            # generate different possibilities for the power
            powerActions, newGameStates = power_actions(card, input["game state"])
            # for each actions
            for i, action in enumerate(powerActions):
                tmpActions = copy.deepcopy(newActions)
                if card['color'] == 'blue' or card['color'] == 'white':
                    tmpActions += action
                else:
                    tmpActions.append(action)

                # copy game state
                newGameState = copy.deepcopy(newGameStates[i])

                for moveId, moveCaseId in enumerate(get_destination_cases(find_player_position(newGameState['characters'], card['color']))):
                    if moveCaseId in newGameState['blocked'] and\
                            find_player_position(newGameState['characters'], card['color']) in newGameState['blocked']:
                        continue
                    # generate output
                    tmpActions = copy.deepcopy(tmpActions)
                    tmpActions.append({'value': moveCaseId, 'type': 'caseId', 'msg': 'move to the case'})
                    tmpActions.append({"value": 0, "type": "choice", "msg": 'before NO power activation'})
                    """
                    Final update
                    """
                    gameStateOut = copy.deepcopy(input)
                    gameStateOut["game state"]['characters'] = update_position(
                        copy.deepcopy(newGameState["characters"]),
                        card['color'],
                        moveCaseId)
                    gameStateOut['game state']['active tiles'] = copy_except(copy.deepcopy(input['game state']['active tiles']), card)
                    leafs.append({
                        "actions": copy.deepcopy(tmpActions),
                        "gamestate": gameStateOut
                    })


            """
                POWER AFTER
            """
            # power after
            newActions = copy.deepcopy(cardActions)
            # no activate power before
            newActions.append({"value": 0, "type": "choice", "msg": 'before No power activation'})
            # generate movement
            for moveId, moveCaseId in enumerate(get_destination_cases(card['position'])):
                if moveCaseId in input['game state']['blocked'] and \
                        find_player_position(input['game state']['characters'], card['color']) in input['game state']['blocked']:
                    continue
                # generate output
                tmpActions = copy.deepcopy(newActions)
                tmpActions.append({'value': moveCaseId, 'type': 'caseId', 'msg': 'move to the case'})
                tmpActions.append({"value": 1, "type": "choice", "msg": 'after Yes power activation'})

                gameStateOut = copy.deepcopy(input)
                gameStateOut["game state"]['characters'] = update_position(
                    copy.deepcopy(gameStateOut["game state"]["characters"]),
                    card['color'],
                    moveCaseId)
                gameStateOut['game state']['active tiles'] = copy_except(copy.deepcopy(input['game state']['active tiles']), card)

                powerActions, newGameStates = power_actions(card, gameStateOut["game state"])
                for i, action in enumerate(powerActions):
                    tmpActionsF = copy.deepcopy(tmpActions)
                    if card['color'] == 'blue' or card['color'] == 'white':
                        tmpActionsF += action
                    else:
                        tmpActionsF.append(action)


                    ttGameState = copy.deepcopy(gameStateOut)
                    ttGameState['game state'] = newGameStates[i]
                    leafs.append({
                        "actions": tmpActionsF,
                        "gamestate": ttGameState
                    })

            """
                POWER No            
            """
            # power after
            newActions = copy.deepcopy(cardActions)
            # no activate power before
            newActions.append({"value": 0, "type": "choice", "msg": 'before No power activation'})
            # generate movement
            for moveId, moveCaseId in enumerate(get_destination_cases(card['position'])):
                if moveCaseId in input['game state']['blocked'] and \
                        find_player_position(input['game state']['characters'], card['color']) in input['game state']['blocked']:
                    continue
                # generate output
                tmpActions = copy.deepcopy(newActions)
                tmpActions.append({'value': moveCaseId, 'type': 'caseId', 'msg': 'move to the case'})

                gameStateOut = copy.deepcopy(input)
                gameStateOut["game state"]['characters'] = update_position(
                    copy.deepcopy(gameStateOut["game state"]["characters"]),
                    card['color'],
                    moveCaseId)
                gameStateOut['game state']['active tiles'] = copy_except(copy.deepcopy(input['game state']['active tiles']), card)

                tmpActions.append({"value": 0, "type": "choice", "msg": 'after No power activation'})

                leafs.append({
                    "actions": tmpActions,
                    "gamestate": gameStateOut
                })


        # generate normal players
        else:
            # move after power before
            if card["color"] in before:
                # no power
                newActions = copy.deepcopy(cardActions)
                newActions.append({"value": 0, "type": "choice", "msg": 'before NO power activation'})
                gen_move(newActions, input['game state'])

                # power
                newActions = copy.deepcopy(cardActions)
                newActions.append({"value": 1, "type": "choice", "msg": 'before YES power activation'})

                powerActions, newGameStates = power_actions(card, input["game state"])
                for i, action in enumerate(powerActions):
                    tmpActions = copy.deepcopy(newActions)
                    tmpActions .append(action)
                    gen_move(tmpActions, newGameStates[i])

            # move before power after
            if card["color"] in after:
                for moveId, moveCaseId in enumerate(get_destination_cases(card['position'])):
                    if moveCaseId in input['game state']['blocked'] and card['position'] in input['game state']['blocked']:
                        continue
                    # generate output
                    tmpActions = copy.deepcopy(cardActions)
                    tmpActions.append({'value': moveCaseId, 'type': 'caseId', 'msg': 'move to the case'})

                    # no power
                    newActions = copy.deepcopy(tmpActions)
                    newActions.append({"value": 0, "type": "choice", "msg": 'before NO power activation'})

                    """
                    Final update
                    """
                    gameStateOut = copy.deepcopy(input)
                    gameStateOut["game state"]['characters'] = update_position(
                        copy.deepcopy(gameStateOut["game state"]["characters"]),
                        card['color'],
                        moveCaseId)
                    gameStateOut['game state']['active tiles'] = copy_except(copy.deepcopy(input['game state']['active tiles']), card)
                    leafs.append({
                        "actions": copy.deepcopy(newActions),
                        "gamestate": gameStateOut
                    })

                    # power after
                    newActions = copy.deepcopy(tmpActions)
                    newActions.append({"value": 1, "type": "choice", "msg": 'before NO power activation'})

                    powerActions, newGameStates = power_actions(card, input["game state"])
                    for i, action in enumerate(powerActions):
                        tmpActions2 = copy.deepcopy(newActions)
                        if card['color'] != "black":
                            if card['color'] == 'white':
                                tmpActions2 += action
                            else:
                                tmpActions2.append(action)

                        ttGameState = copy.deepcopy(gameStateOut)
                        ttGameState['game state'] = newGameStates[i]
                        leafs.append({
                            "actions": tmpActions2,
                            "gamestate": ttGameState
                        })
    if phantom:
        phantom_heurisitic(leafs)
    else:
        inspector_heurisitic(leafs)
    leafs.sort(key=lambda x: x['score'], reverse=True)
    return leafs



def draw_rect(canvas, x, y, width, height, color=""):
    canvas.create_rectangle(x, y, x + width, y + height, fill=color)


def get_color(color_name):
    colors = {
        "purple": "#6a0dad",
        "white": "#ffffff",
        "black": "#000000",
        "brown": "#964B00",
        "grey": "#d3d3d3",
        "pink": "#ffc0cb",
        "blue": "#0000FF",
        "red": "#FF0000"
    }
    return colors[color_name]


import random


def draw(input, canvas, baseY=0, baseX=0):
    array = [
        [0, 1, 1, 0],
        [1, 1, 1, 1],
        [1, 1, 1, 1]
    ]

    # x y
    relation = [
        [3, 3],
        [2, 3],
        [1, 3],
        [0, 3],
        [3, 2],
        [2, 2],
        [1, 2],
        [0, 2],
        [2, 0],
        [1, 0]
    ]

    for lineId, line in enumerate(array):
        for caseId, case in enumerate(line):
            if case == 1:
                draw_rect(canvas, baseX + 100 * caseId, baseY + lineId * 100, 100, 100)

    for player in input['game state']['characters']:
        x_new = (relation[player['position']][0] + 1) * 100 - 100
        y_new = (relation[player['position']][1] + 1) * 100 - 200
        if relation[player['position']][1] == 0:
            y_new = 0
        x_new = random.randrange(x_new, x_new + 100 - 10)
        y_new = random.randrange(y_new, y_new + 100 - 10)
        draw_rect(canvas, baseX + x_new, baseY + y_new, 10, 10, get_color(player['color']))

def nb_of_player_at_a_caseId(caseId, players):
    nb = 0
    for player in players:
        if player['position'] == caseId:
            nb += 1
    return nb

# try to maximise the distance between players
# score is the sum of distance between other players

# if the player is alone +10
# if the player have player in this room -100
# if the player have no one player around him + 10
# if the player have player around the case +1

def phantom_at_shadow_case(case_id, players, phantom_color):
    for player in players:
        if player['position'] == case_id and player['color'] == phantom_color:
            return True
    return False


def get_phantom_score(gamestate):
    score = 0
    for player in gamestate['game state']['characters']:

        player_score = 0
        nbPeopleAround = 0
        player_position = player['position']
        # get the distance between him and the other player
        # for i, destcase in enumerate(get_destination_cases(player_position, player['color'])):
        #     nb = nb_of_player_at_a_caseId(destcase, gamestate['game state']['characters'])
        #     nbPeopleAround += nb
        # #player_score += nbPeopleAround
        # if nbPeopleAround == 0:
        #     player_score += 100
        nbAtCase = nb_of_player_at_a_caseId(player['position'], gamestate['game state']['characters'])
        isFantom = False
        isShadowCase = False

        if player['positon'] == gamestate['game state']['shadow']:
            isShadowCase = True

        if player['color'] == gamestate['game state']['fantom']:
            isFantom = True

        # if nbAtCase <= 1 and isFantom:
        #     score += 100
        #
        # if nbAtCase <= 1 and not isFantom:
        #     score += 1
        #
        # if nbAtCase > 1:
        #     score -= 10

        if nbAtCase <= 1:
            score += 1

        if nbAtCase and not isShadowCase:
            score -= 5

        #
        # # multiple players in shadow case without the phantom player
        # if isShadowCase and not phantom_at_shadow_case(gamestate['game state']['shadow'],
        #                                                gamestate['game state']['characters'],
        #                                                gamestate['game state']['fantom']):
        #     player_score -= 1000
        #
        # # multiple players a case
        # if nbAtCase > 1 and not isShadowCase:
        #     player_score -= 1000

        score += player_score
    return score

def phantom_heurisitic(leafs):
    for leaf in leafs:
        leaf['score'] = get_phantom_score(leaf['gamestate'])

# try to minimise the distance between players
# try to make cluster of 2 players
def get_inspector_score(gamestate):
    score = 0
    for player in gamestate['game state']['characters']:
        player_score = 0
        nb = nb_of_player_at_a_caseId(player['position'], gamestate['game state']['characters'])
        if nb > 1 and nb <= 2:
            player_score += 100
        if nb > 2:
            player_score += 50
        score += player_score
    return 0

def inspector_heurisitic(leafs):
    for leaf in leafs:
        leaf['score'] = get_inspector_score(leaf['gamestate'])

# root = tkinter.Tk()
# root.minsize(1200, 720)
# canvas = tkinter.Canvas(root)
# canvas.config(width=1200, height=720)
#
leafs = generate_leaf(input)
#
print("leafs", len(leafs))
#
# print("permanents", permanents)
# print("two", two)
# print("before", before)
# print("after", after)
# print("colors", colors)
#
# leafId = 0

def drawUpdate(event):
    print("DRAW UPDATE")
    global leafId
    leafId += 1
    leafId %= len(leafs)

    canvas.delete("all")

    draw(input, canvas)

    draw(leafs[leafId]['gamestate'], canvas, 100 * 4, 0)
    print(leafs[leafId]['actions'], len(leafs[leafId]['actions']))

    draw(leafs[leafId + 1]['gamestate'], canvas, 0, 100 * 5)
    print(leafs[leafId + 1]['actions'], len(leafs[leafId + 1]['actions']))

    draw(leafs[leafId + 2]['gamestate'], canvas, 100 * 4, 100 * 5)
    print(leafs[leafId + 2]['actions'], len(leafs[leafId + 2]['actions']))

# drawUpdate(None)
#
# root.bind('x', drawUpdate)
# canvas.pack()

# import pandas as pd
#
# color = 0
# def filterColor(tab):
#     return tab['actions'][0]['value'] == color
# print(input['data'][color], len(list(filter(filterColor, leafs))))
# color = 1
# def filterColor(tab):
#     return tab['actions'][0]['value'] == color
# print(input['data'][color], len(list(filter(filterColor, leafs))))
# color = 2
# def filterColor(tab):
#     return tab['actions'][0]['value'] == color
# print(input['data'][color], len(list(filter(filterColor, leafs))))
# color = 3
# def filterColor(tab):
#     return tab['actions'][0]['value'] == color
# print(input['data'][color], len(list(filter(filterColor, leafs))))
#
# pd.DataFrame(leafs)
#
# canvas.mainloop()


