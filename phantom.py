import copy
import socket
import os
import logging
from logging.handlers import RotatingFileHandler
import json

import ai_engine
import protocol
from random import randrange
import random

host = "localhost"
port = 12000
# HEADERSIZE = 10

"""
set up fantom logging
"""
fantom_logger = logging.getLogger()
fantom_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s :: %(levelname)s :: %(message)s", "%H:%M:%S")
# file
if os.path.exists("./logs/fantom.log"):
    os.remove("./logs/fantom.log")
file_handler = RotatingFileHandler('./logs/fantom.log', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
fantom_logger.addHandler(file_handler)
# stream
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.WARNING)
fantom_logger.addHandler(stream_handler)


# determines whether the power of the character is used before
# or after moving
permanents, two, before, after = {'pink'}, {
    'red', 'grey', 'blue'}, {'purple', 'brown'}, {'black', 'white'}
# reunion of sets
colors = before | permanents | after | two
# ways between rooms
passages = [{1, 4}, {0, 2}, {1, 3}, {2, 7}, {0, 5, 8},
            {4, 6}, {5, 7}, {3, 6, 9}, {4, 9}, {7, 8}]
# ways for the pink character
pink_passages = [{1, 4}, {0, 2, 5, 7}, {1, 3, 6}, {2, 7}, {0, 5, 8, 9}, {
    4, 6, 1, 8}, {5, 7, 2, 9}, {3, 6, 9, 1}, {4, 9, 5}, {7, 8, 4, 6}]

print(colors)
print(passages)
print(pink_passages)


class Player():

    def __init__(self):
        self.end = False
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.currentListOfActions = []
        self.currentActionId = -1
        self.questionsType = {}

    def connect(self):
        self.socket.connect((host, port))

    def reset(self):
        self.socket.close()

    def action_choose(self, actionId, data):
        action = self.currentListOfActions[actionId]
        print("==> action to do", self.currentListOfActions[actionId])
        print("==> input datas", data)
        if action['type'] == 'cardId':
            return action['value']

        if action['type'] == 'caseId':
            try:
                return data.index(action['value'])
            except Exception as e:
                pass
        if action['value'] == -1 or action['type'] == 'undefined':
            return random.randint(0, len(data)-1)

        if action['type'] == 'choice':
            return action['value']

        if action['type'] == 'playerSelection':
            try:
                return data.index(action['value'])
            except Exception as e:
                pass

        return random.randint(0, len(data) - 1)


    def answer(self, question):
        # work
        data = question["data"]
        game_state = question["game state"]
        response_index = random.randint(0, len(data)-1)
        print("json data ==>", json.dumps(question, indent=4, sort_keys=True))
        print("json data ==>", json.dumps(question['data'], indent=4, sort_keys=True))
        print("json question type ==>", json.dumps(question['question type'], indent=4, sort_keys=True))

        self.questionsType[question['question type']] = 1

        self.currentActionId += 1
        if question['question type'] == "select character":
            self.currentListOfActions = None
            self.currentActionId = 0
        if not self.currentListOfActions:
            ai_result = ai_engine.generate_leaf(copy.deepcopy(question))
            # choose the best action
            print("==> All Actions to do", ai_result[0]['actions'])
            print("==> Score of this", ai_result[0]['score'])
            self.currentListOfActions = ai_result[0]['actions']

        try:
            response_index = self.action_choose(self.currentActionId, question["data"])
        except Exception as e:
            response_index = random.randint(0, len(data) - 1)
        # pass the phantom in random mod
        #response_index = random.randint(0, len(data) - 1)
        print("Response index", response_index)
        #response_index = int(input("Enter value in "))
        # log
        fantom_logger.debug("|\n|")
        fantom_logger.debug("fantom answers")
        fantom_logger.debug(f"question type ----- {question['question type']}")
        fantom_logger.debug(f"data -------------- {data}")
        fantom_logger.debug(f"response index ---- {response_index}")
        fantom_logger.debug(f"response ---------- {data[response_index]}")
        print(self.questionsType)
        return response_index

    def handle_json(self, data):
        data = json.loads(data)
        response = self.answer(data)
        # send back to server
        bytes_data = json.dumps(response).encode("utf-8")
        protocol.send_json(self.socket, bytes_data)

    def run(self):

        self.connect()

        while self.end is not True:
            received_message = protocol.receive_json(self.socket)
            if received_message:
                self.handle_json(received_message)
            else:
                print("no message, finished learning")
                self.end = True


p = Player()

p.run()


# Score
#

# Score max if phantom tout seul
# Score max if phantom tout seul et distance des autre joueur grand
# Score max if phantom n'est pas le seul a etre tout seul

# strategy
# eparpiller les joueurs
# essayer d'etre tout seul ou a plusieur dans une chambre sombre


# 0.5 * distance avec les autres joueur + 0.2 * distance_salle_sombre + if tout seul + 100 +